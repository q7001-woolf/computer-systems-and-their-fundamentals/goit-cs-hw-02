# Визначення масиву з URL вебсайтів
sites=("https://google.com" "https://facebook.com" "https://www.edu.goit.global")

# Визначення назви файлу логів
logfile="website_availability.log"

# Очищення файлу логів перед записом нових результатів
> "$logfile"

# Перевірка кожного сайту
for site in "${sites[@]}"; do
  # Використання curl для отримання HTTP статус-коду
  status=$(curl -s -L -o /dev/null -w "%{http_code}" "$site")
  
  # Перевірка статус-коду та запис результату у файл логів
  if [[ "$status" -eq 200 ]]; then
    echo "$site is UP" >> "$logfile"
  else
    echo "$site is DOWN" >> "$logfile"
  fi
done

# Вивід повідомлення про запис у файл логів
echo "Results have been logged to $logfile"
